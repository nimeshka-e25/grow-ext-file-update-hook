"""Setup script for extension."""

from setuptools import setup

setup(
    name='grow-ext-file-update-hook',
    version='1.0.1',
    license='MIT',
    author='E25',
    author_email='@',
    include_package_data=False,
    packages=[
        'file_update_hook',
    ],
    install_requires=[],
)
