"""File Update Hook Extension"""

from grow import extensions
from grow.extensions import hooks

class GrowFileUpdateRegisterHook(hooks.DevFileChangeHook):
    """Handle the file update hook."""

    def should_trigger(self, previous_result, *_args, **_kwargs):
        """Determine if the hook should trigger."""
        return self.extension.config.get('enabled', True)

    def trigger(self, previous_result, pod_path, *_args, **_kwargs):
        """Trigger the deployment destination registration hook."""
        sys.exit('Hook triggered')

class GrowFileUpdateHookExtension(extensions.BaseExtension):
    """File Update Hook Extension"""

    @property
    def available_hooks(self):
        """Returns the available hook classes."""
        return [GrowFileUpdateRegisterHook]
